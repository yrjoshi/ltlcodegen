CXXFLAGS =	-O0 -g -Wall -fmessage-length=0

OBJS =		LTLParser.o

LIBS =

TARGET =	LTLParser

$(TARGET):	$(OBJS)
	$(CXX) -o $(TARGET) $(OBJS) $(LIBS)

all:	$(TARGET)

clean:
	rm -f $(OBJS) $(TARGET)
