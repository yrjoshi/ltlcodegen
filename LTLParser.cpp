//============================================================================
// Name        : LTLParser.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C, Ansi-style
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <utility>
#include <string.h>
#include <iostream>
#include <set>
#include <fstream>
using namespace std;

pair<int,int> search_bracket(char *str)
{
	pair<int,int> retval;
	retval.first = -1;
	retval.second = -1;
	for(int i = 0;i < strlen(str);i++)
	{
		if(str[i] == '(')
			retval.first = i;
		if(str[i] == ')')
		{
			retval.second = i;
			break;
		}
	}
//	cout<<retval.first<<"\t"<<retval.second<<endl;
	return retval;
}

set <string> inprogress[10];
static int inprogress_count = 0;
static int queue_count;
vector <string> kernel_list;
vector <string> bufferlist;
vector <string> predlist;
static int max_queue_count = 0;
void code_gen(char *subform, int tracelen, ofstream &codefile)
{

	char formula[50]={NULL};
	string firstpred(""), secondpred(""), ltloperator(""),resultpred("");
	bool pred_eval = false, unary = false, binary = false;
	strncpy(formula,subform,strlen(subform));

	string form(formula);
//	cout<<form<<endl;
	int one = form.find("(");
	int two = form.find(" ");
	if(two == string::npos)
	{
		two = form.find("<>");
		if(two == string::npos)
		{
			two = form.find("[]");
			if(two == string::npos)
			{
				two = form.find("!");
				if(two == string::npos)
				{
					two = form.find("X");
					if(two != string::npos)
					{
						ltloperator="X";
						unary = true;
					}
				}
				else
				{
					ltloperator="!";
					unary = true;
				}
			}
			else
			{
				ltloperator="[]";
				unary = true;
			}
		}
		else
		{
			ltloperator="<>";
			unary = true;
		}
		if(two != string::npos)
		{
			firstpred = form.substr(two+2,2);
		}
	}
	else
	{
		firstpred = form.substr(one+1,two-one-1);
	}

	int three = form.rfind(")");
	int four = form.rfind(" ");
	if(four != string::npos)
	{
		secondpred = form.substr(four+1,three-four-1);
		ltloperator = form.substr(two+1,four-two-1);
		binary=true;
	}
	else
	{
		if(!unary)
		{
			firstpred = form.substr(one+1,three-one-1);
			pred_eval = true;
		}
	}

	int five = form.find("=");
	resultpred = form.substr(0,five);
	if(pred_eval)
	{
		codefile<<"//*********************************************************************\n";
		codefile<<"//evaluating "<<firstpred<<"\n";
		codefile<<"//*********************************************************************\n";

		codefile<<"long *"<<resultpred<<";\n";
		codefile<<"long *"<<firstpred<<",*"<<firstpred<<"_host"<<";\n";
		codefile<<firstpred<<"_host = (long *)malloc("<<tracelen<<"*sizeof(long));\n";
		if(predlist.size() == 0)
		{
			codefile<<"cudaStream_t stream[5];\n";
			codefile<<"for(int i = 0; i<5;i++)\n";
			codefile<<"\tcudaStreamCreate(&stream[i]);\n";
			codefile<<"cudaError_t err;\n";
			codefile<<"thrust::minimum<long> min_op;\n";
			codefile<<"thrust::maximum<long> max_op;\n";
		}
		codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
		codefile<<"if (err != cudaSuccess)\n{\n";
		codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
		codefile<<"return err;\n}\n";

		codefile<<"err = cudaMalloc((void**)&"<<firstpred<<",sizeof(long)*"<<tracelen<<");\n";
		codefile<<"if (err != cudaSuccess)\n{\n";
		codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
		codefile<<"return err;\n}\n";

		codefile<<"//copy contents of "<<firstpred<<"\n";

		codefile<<"err = cudaMemcpyAsync("<<firstpred<<","<<firstpred<<"_host,sizeof(long)*"<<tracelen<<",cudaMemcpyHostToDevice,stream["<<queue_count<<"]);\n";
		codefile<<"if (err != cudaSuccess)\n{\n";
		codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
		codefile<<"return err;\n}\n";
		if(predlist.size() == 0)
			codefile<<"int blocksPerGrid = "<<tracelen<<"/512; int threadsPerBlock = 512;\n";
		codefile<<"kern_"<<firstpred<<"<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<firstpred<<","<<resultpred<<","<<tracelen<<");\n";
		codefile<<"err = cudaGetLastError();\n";
		codefile<<"if (err != cudaSuccess)\n{\n";
		codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
		codefile<<"return err;\n}\n";

		kernel_list.push_back("kern_"+firstpred);
		bufferlist.push_back(resultpred);
		bufferlist.push_back(firstpred);
		predlist.push_back(firstpred+"_host");
		inprogress[inprogress_count].insert(firstpred);
		inprogress[inprogress_count].insert(resultpred);
	}
	else
	{
		cout<<firstpred<< " "<<resultpred<<endl;
		for(int m = 0; m <= inprogress_count;m++)
			if(inprogress[m].find(firstpred) != inprogress[m].end()
			|| inprogress[m].find(secondpred) != inprogress[m].end())
			{

				if(queue_count >= 0 && m == inprogress_count )
				{
					if(queue_count > max_queue_count)
						max_queue_count = queue_count;
					for(int j = 0; j < queue_count;j++)
						codefile<<"cudaStreamSynchronize(stream["<<j<<"]);\n";
					queue_count = 0;
					inprogress_count++;
					break;
				}

			}
	}
	if(unary)
	{
		if(ltloperator == "!")
		{
			codefile<<"//*********************************************************************\n";
			codefile<<"//evaluating !"<<firstpred<<"\n";
			codefile<<"//*********************************************************************\n";

			codefile<<"long *"<<resultpred<<";\n";

			codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";


			kernel_list.push_back(firstpred+"_not_kern");

			codefile<<"not_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<firstpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
		}
		if(ltloperator == "X")
		{
			codefile<<"//*********************************************************************\n";
			codefile<<"//evaluating X"<<firstpred<<"\n";
			codefile<<"//*********************************************************************\n";

			codefile<<"long *"<<resultpred<<";\n";

			codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";


			kernel_list.push_back(firstpred+"_next_kern");

			codefile<<"next_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<firstpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
		}
		if(ltloperator == "<>")
		{
			codefile<<"//*********************************************************************\n";
			codefile<<"//evaluating <>"<<firstpred<<"\n";
			codefile<<"//*********************************************************************\n";
			codefile<<"long *"<<resultpred<<";\n";

			codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";

			kernel_list.push_back(firstpred+"_F_kern");

			codefile<<"F_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count<<"]>>>("<<firstpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
			codefile<<"cudaStreamSynchronize(stream["<<queue_count<<"]);\n";

			codefile<<"thrust::device_ptr<long>"<<resultpred<<"_ptr("<<resultpred<<");\n";
			codefile<<"long "<<resultpred<<"_min = thrust::reduce("<<resultpred<<"_ptr, "<<resultpred<<"_ptr+MAX,(long) (MAX+1),min_op);\n";

			codefile<<"set_F_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<resultpred<<",&"<<resultpred<<"_min,"<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";

		}
		if(ltloperator == "[]")
		{
			codefile<<"//*********************************************************************\n";
			codefile<<"//evaluating []"<<firstpred<<"\n";
			codefile<<"//*********************************************************************\n";

			codefile<<"long *"<<resultpred<<";\n";

			codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";

			kernel_list.push_back(firstpred+"_G_kern");

			codefile<<"G_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count<<"]>>>("<<firstpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
			codefile<<"cudaStreamSynchronize(stream["<<queue_count<<"]);\n";

			codefile<<"thrust::device_ptr<long>"<<resultpred<<"_ptr("<<resultpred<<");\n";
			codefile<<"long "<<resultpred<<"_max = thrust::reduce("<<resultpred<<"_ptr, "<<resultpred<<"_ptr+MAX,(long) -1,max_op);\n";

			codefile<<"set_G_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<resultpred<<",&"<<resultpred<<"_max,"<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
		}
		bufferlist.push_back(resultpred);
	}
	if(binary)
	{
		if(ltloperator == "U")
		{
			codefile<<"//*********************************************************************\n";
			codefile<<"//evaluating "<<firstpred<<" U "<<secondpred<<" \n";
			codefile<<"//*********************************************************************\n";

			codefile<<"long *"<<resultpred<<";\n";

			codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";

			kernel_list.push_back(firstpred+"_U_kern");

			codefile<<"U1_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count<<"]>>>("<<firstpred<<","<<secondpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
			codefile<<"cudaStreamSynchronize(stream["<<queue_count<<"]);\n";

			codefile<<"thrust::device_ptr<long>"<<resultpred<<"_ptr("<<resultpred<<");\n";
			codefile<<"long "<<resultpred<<"_min = thrust::reduce("<<resultpred<<"_ptr, "<<resultpred<<"_ptr+MAX,(long) (MAX+1),min_op,"<<tracelen<<");\n";

			codefile<<"U2_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count<<"]>>>("<<firstpred<<","<<secondpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
			codefile<<"cudaStreamSynchronize(stream["<<queue_count<<"]);\n";

			codefile<<"thrust::device_ptr<long>"<<resultpred<<"_ptr("<<resultpred<<");\n";
			codefile<<"long "<<resultpred<<"_max = thrust::reduce("<<resultpred<<"_ptr, "<<resultpred<<"_ptr+"<<resultpred<<"_min,(long) -1,max_op);\n";

			codefile<<"set_U_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<resultpred<<",&"<<resultpred<<"_max,"<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
		}
		if(ltloperator == "->")
		{
			codefile<<"//*********************************************************************\n";
			codefile<<"//evaluating "<<firstpred<<" -> "<<secondpred<<" \n";
			codefile<<"//*********************************************************************\n";

			codefile<<"long *"<<resultpred<<";\n";

			codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";

			kernel_list.push_back(firstpred+"_i_kern");

			codefile<<"if_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<firstpred<<","<<secondpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";


		}
		if(ltloperator == "&&")
		{
			codefile<<"//*********************************************************************\n";
			codefile<<"//evaluating "<<firstpred<<" && "<<secondpred<<" \n";
			codefile<<"//*********************************************************************\n";

			codefile<<"long *"<<resultpred<<";\n";

			codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";

			kernel_list.push_back(firstpred+"_i_kern");

			codefile<<"and_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<firstpred<<","<<secondpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
		}
		if(ltloperator == "||")
		{
			codefile<<"//*********************************************************************\n";
			codefile<<"//evaluating "<<firstpred<<" || "<<secondpred<<" \n";
			codefile<<"//*********************************************************************\n";

			codefile<<"long *"<<resultpred<<";\n";

			codefile<<"err = cudaMalloc((void**)&"<<resultpred<<",sizeof(long)*"<<tracelen<<");\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";

			kernel_list.push_back(firstpred+"_i_kern");

			codefile<<"or_kern<<<blocksPerGrid, threadsPerBlock,0,stream["<<queue_count++<<"]>>>("<<firstpred<<","<<secondpred<<","<<resultpred<<","<<tracelen<<");\n";
			codefile<<"err = cudaGetLastError();\n";
			codefile<<"if (err != cudaSuccess)\n{\n";
			codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
			codefile<<"return err;\n}\n";
		}
		bufferlist.push_back(resultpred);
	}

//	cout<<firstpred<<secondpred<<ltloperator<<resultpred<<endl;
}
void gen_final_val(char *formula,int tracelen, ofstream &codefile)
{
	codefile<<"//*********************************************************************\n";
	codefile<<"//evaluating final verdict\n";
	codefile<<"//*********************************************************************\n";

	codefile<<"long *final_verdict;\n";
	codefile<<"final_verdict = (long *)malloc("<<tracelen<<"*sizeof(long));\n";

	codefile<<"err = cudaMemcpy(final_verdict,"<<formula<<",sizeof(long)*"<<tracelen<<", cudaMemcpyDeviceToHost);\n";
	codefile<<"if (err != cudaSuccess)\n{\n";
	codefile<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
	codefile<<"return err;\n}\n";
	kernel_list.push_back("final_val");

}
int main(int argc, char *argv[])
{
	char *formula = argv[1]; int tracelen;
	ofstream plan_file;
	plan_file.open(argv[2]);
	tracelen = atoi(argv[3]);
	unsigned count = 0;
	char formulas[3][50]={NULL,NULL,NULL};
	char subform[50]={""};

	while(search_bracket(formula).first != -1 )
	{
		pair<int,int> result = search_bracket(formula);
		strncpy(formulas[0]," ",strlen(formulas[0]));
		strncpy(formulas[1]," ",strlen(formulas[1]));
		strncpy(formulas[2]," ",strlen(formulas[2]));
		strncpy(subform," ",strlen(subform));
		if(result.first > 0)
			strncpy(formulas[0],&formula[0],result.first);
		else
			strcpy(formulas[0],"");

		strncpy(formulas[1],&formula[result.first],result.second-result.first + 1);
		sprintf(subform,"F%d=%s",count,formulas[1]);

		if(result.second+1 < strlen(formula))
			strncpy(formulas[2],&formula[result.second+1],strlen(formula)-result.second+1);
		else
			strcpy(formulas[2],"");

//		plan_file<<subform;
//		plan_file<<formulas[0]<<" F"<<count<<" "<<formulas[2];
		cout<<subform<<endl;
//		cout<<formulas[0]<<" F"<<count<<" "<<formulas[2]<<endl;
		sprintf(formula,"%sF%d%s",formulas[0],count,formulas[2]);
		code_gen(subform, tracelen, plan_file);
//		cout<<formula<<endl;
		count++;
	}
	for(int j = 0; j < queue_count;j++)
		plan_file<<"cudaStreamSynchronize(stream["<<j<<"]);\n";

	gen_final_val(formula,tracelen,plan_file);

//	for(int j = 0; j <	kernel_list.size();j++)
//		plan_file<<"clReleaseKernel("<<kernel_list.at(j)<<");\n";
	for(int k = 0; k < max_queue_count;k++)
		plan_file<<"cudaStreamSynchronize(stream["<<k<<"]);\n";

	for(int k = 0; k < bufferlist.size();k++)
	{
		plan_file<<"cudaFree("<<bufferlist.at(k)<<");\n";
		plan_file<<"if (err != cudaSuccess)\n{\n";
		plan_file<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
		plan_file<<"return err;\n}\n";
	}
	for(int k = 0; k < predlist.size();k++)
			plan_file<<"free("<<predlist.at(k)<<");\n";

	plan_file<<"for(int i = 0; i<5;i++)\n";
	plan_file<<"\tcudaStreamDestroy(stream[i]);\n";
	plan_file<<"cudaError_t err;\n";

	plan_file<<"err = cudaDeviceReset();\n";
	plan_file<<"if (err != cudaSuccess)\n{\n";
	plan_file<<"fprintf(stderr,\"%s at %u \\n\",cudaGetErrorString(err),__LINE__);\n";
	plan_file<<"return err;\n}\n";
	plan_file.close();
}
